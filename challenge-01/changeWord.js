function changeWord(selectedText, changeText, text){
    text = text.split(" ")
    let test = text.indexOf(selectedText)
    text.splice(test, 1, changeText)
    return text.join(" ")
  }
  
const kalimat1 = 'Andini sangat mencintai kamu selamanya'
const kalimat2 = 'Gunung bromo tak akan mampu menggambarkan besarnya cintaku padamu'
  
console.log(changeWord('mencintai', 'membenci', kalimat1))
  
console.log(changeWord('bromo', 'semeru', kalimat2))