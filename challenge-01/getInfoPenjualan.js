const dataPenjualanNovel = [
    {
      idProduct: 'BOOK002421',
      namaProduk: 'Pulang - Pergi',
      penulis: 'Tere Liye',
      hargaBeli: 60000,
      hargaJual: 86000,
      totalTerjual: 150,
      sisaStok: 17,
    },
    {
      idProduct: 'BOOK002351',
      namaProduk: 'Selamat Tinggal',
      penulis: 'Tere Liye',
      hargaBeli: 75000,
      hargaJual: 103000,
      totalTerjual: 171,
      sisaStok: 20,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Garis Waktu',
      penulis: 'Fiersa Besari',
      hargaBeli: 67000,
      hargaJual: 99000,
      totalTerjual: 213,
      sisaStok: 5,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Laskar Pelangi',
      penulis: 'Andrea Hirata',
      hargaBeli: 55000,
      hargaJual: 68000,
      totalTerjual: 20,
      sisaStok: 56,
    },
  ];

function getInfoPenjualan(dataPenjualan){
    var keuntungan = 0
    var modal = 0
    var fam = 0

    for (let i in dataPenjualan){
        if(typeof dataPenjualan[i] === "object"){
            keuntungan += (dataPenjualan[i].hargaJual - dataPenjualan[i].hargaBeli) * dataPenjualan[i].totalTerjual;
            modal += (dataPenjualan[i].totalTerjual + dataPenjualan[i].sisaStok) * dataPenjualan[i].hargaBeli;
            if(dataPenjualan[i].totalTerjual > fam){
                fam = dataPenjualan[i].totalTerjual
                produk = dataPenjualan[i]
                penulis = dataPenjualan[i]
        }
        } else {
            return Error("Invalid Data Type")
        }
        
    }
    var penjualan = ((modal/keuntungan) * 100).toFixed() + "%";
    var keuntungan = Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(keuntungan)
    var modal = Intl.NumberFormat("id-ID", { style: "currency", currency: "IDR" }).format(modal)
    
    infoPenjualan = {totalKeuntungan: keuntungan, totalModal: modal, presentasePenjualan: penjualan, produkBukuTerlaris: "BUKU TERLARIS BERDASARKAN DATA DIATAS ADALAH "+produk.namaProduk, penulisTerlaris: "PENULIS TERLARIS BERDASARKAN DATA DIATAS ADALAH "+produk.penulis}

return infoPenjualan
}

console.log(getInfoPenjualan(dataPenjualanNovel))