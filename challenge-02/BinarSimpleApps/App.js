import React from 'react';
import {
    StyleSheet,
    View,
    Text, 
    Image, 
    Dimensions
} from 'react-native';


const App = () =>  {
    return(
        <View style={styles.container}>
            <View style={styles.card}>
                <Image
                    style={styles.image}
                    resizeMode="cover"
                    source={{
                        uri:"https://res.cloudinary.com/djyjm9ayd/image/upload/v1643249117/19025216_1439860629412992_3671167199250762358_o_hzryz8.png"
                    }} />
                <View>
                    <Text style={styles.titleText}>Styling React Native</Text>
                    <Text style={styles.subText}>Binar Academy - React Native</Text>
                    <Text style={styles.followText}>Lorem Ipsum Dolor Sit Amet Lorem, Lorem Lorem Lorem Lorem</Text>
                </View>
                <View style={styles.footer}>
                <Text style={styles.footerText}>Understood!</Text>
                <Text style={styles.footerText}>What?</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
        alignItems: "center",
        justifyContent: "center",
    },
    card: {
        backgroundColor: "#FFFFFF",
        width: 182,
        height: 272,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    }, 
    image:{
        justifyContent: "center",
        width: 182,
        height: 180,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    titleText: {
        textAlign: "center",
        justifyContent: "center",
        fontWeight: "bold",
        fontSize: 12,
        color: "#000000",
    },
    subText: {
        textAlign: "center",
        justifyContent: "center",
        fontSize: 8,
        color: "#656565"
    },
    followText:{
        color: "#000000",
        fontSize: 8,
    },
    footerText:{
        fontSize: 10,
        color: "#06B04D",
    },
    footer:{
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: 15,
        paddingVertical: 10
    }
})


export default App;
