import logo from './logo.svg';
import React from 'react';
import './App.css';
import {todo} from './todo.js';
import {
  CreateTodo,
  TodoList,
  TodoListHead, 
  TodoListItems
} from "./Components";

export default class App extends React.Component {
  render() {
    return(
      <div id='app'>
        <h1>TODOs</h1>
        <CreateTodo />
        <TodoList>
          {/*<TodoListItems item={todo.data[0]} />*/}
        </TodoList>
      </div>
    )
  }
  
}
