import React from 'react';
import css from './App.css';
import todo from './todo.js';

export class TodoListHead extends React.Component{
    render(){
        return(
            <thead>
                <tr>
                    <th>Task</th>
                    <th>Action</th>
                </tr>
            </thead>
        )
    }
}


export class TodoList extends React.Component{
    render(){
        
        return(
            <table>
                <TodoListHead />
                <tbody>
                    {/*{this.renderTodoListItems()}*/}
                </tbody>
            </table>
        )
    }
}

export class TodoListItems extends React.Component{
    render(){
        return(
            <tr>
                <td>{this.props.item.task}</td>
                <td>
                    <button>Delete</button>
                </td>
            </tr>
        )
    }
}

export class CreateTodo extends React.Component{
    render(){
        return(
            <div>
                <form className='create-todo-form'>
                    <input type="text" name="task" placeholder="Add a task" />
                    <button type="submit">Add</button>
                </form>
            </div>
        )
    }
}
